﻿//
//  Copyright (C) 2011 - 2012, blugri Software + Services BVBA.
//  All rights reserved
//
using System;
using System.ComponentModel.Composition;
using Microsoft.WebAnalytics;

namespace Analytics
{
    public class AnalyticsTracker
    {

        private AnalyticsService _service;

        public AnalyticsTracker(AnalyticsService service)
        {
            CompositionInitializer.SatisfyImports(this);

            _service = service;
        }

        [Import("Log")]
        public Action<AnalyticsEvent> Log { get; set; }

        public void Track(string category, string name)
        {
            Track(category, name, null);
        }

        public void Track(string category, string name, string actionValue)
        {
            Log(new AnalyticsEvent { Category = category, Name = name, ObjectName = actionValue });
        }

        public void TrackPageView(string pageViewName)
        {
            // Avoid double tracking
            if (_service != null && !_service.AutomaticPageTrackingEnabled)
            {
                Log(new AnalyticsEvent
                {
                    ActionValue = pageViewName,
                    HitType = Microsoft.WebAnalytics.Data.HitType.PageView,
                    Name = "CurrentScreenChanged",
                    NavigationState = pageViewName,
                    ObjectType = pageViewName,
                    AppName = Analytics.AnalyticsProperties.ApplicationTitle
                });
            }
        }
    }
}
