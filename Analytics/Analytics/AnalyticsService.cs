﻿//
//  Copyright (C) 2011 - 2012, blugri Software + Services BVBA.
//  All rights reserved
//
using System.ComponentModel.Composition.Hosting;
using System.Windows;
using Google.WebAnalytics;
using Microsoft.WebAnalytics;
using Microsoft.WebAnalytics.Behaviors;
using Microsoft.WebAnalytics.Data;

namespace Analytics
{
    public class AnalyticsService : IApplicationService
    {
        private readonly IApplicationService _innerService;
        private readonly GoogleAnalytics _googleAnalytics;

        public AnalyticsService()
        {
            _googleAnalytics = new GoogleAnalytics();
            _googleAnalytics.CustomVariables.Add(new PropertyValue { PropertyName = "Device ID", Value = AnalyticsProperties.DeviceId });
            _googleAnalytics.CustomVariables.Add(new PropertyValue { PropertyName = "Application", Value = AnalyticsProperties.Application });
            _googleAnalytics.CustomVariables.Add(new PropertyValue { PropertyName = "Device OS", Value = AnalyticsProperties.OsVersion });
            _googleAnalytics.CustomVariables.Add(new PropertyValue { PropertyName = "Device", Value = AnalyticsProperties.Device });
            _innerService = new WebAnalyticsService
            {
                IsPageTrackingEnabled = AutomaticPageTrackingEnabled,
                Services = { _googleAnalytics, }
            };
        }

        public string WebPropertyId
        {
            get { return _googleAnalytics.WebPropertyId; }
            set { _googleAnalytics.WebPropertyId = value; }
        }

        public bool AutomaticPageTrackingEnabled 
        {
            get 
            {
                if (_innerService != null)
                {
                    return ((WebAnalyticsService)_innerService).IsPageTrackingEnabled;
                }
                return false;
            }
            set 
            { 
                ((WebAnalyticsService)_innerService).IsPageTrackingEnabled = value; 
            }
        }

        #region IApplicationService Members

        public void StartService(ApplicationServiceContext context)
        {
            CompositionHost.Initialize(
                new AssemblyCatalog(
                    Application.Current.GetType().Assembly),
                new AssemblyCatalog(typeof(AnalyticsEvent).Assembly),
                new AssemblyCatalog(typeof(TrackAction).Assembly));
            _innerService.StartService(context);
        }

        public void StopService()
        {
            _innerService.StopService();
        }

        #endregion
    }
}
